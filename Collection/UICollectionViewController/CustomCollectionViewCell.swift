//
//  CustomCollectionViewCell.swift
//  UICollectionViewController
//
//  Created by Родион Шашенко on 02.04.2023.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    static let identifier = "CustomCollectionViewCell"
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(imageView)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageView.frame = contentView.bounds
    }
    
    var image: UIImage? {
        didSet{
            imageView.image = image
        }
    }
//    var image: Image? {
//        didSet{
//            guard let imageName = image?.imageName else {return}
//            imageView.image = UIImage(named: imageName)
//        }
//    }

}
