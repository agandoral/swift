//
//  AddFrendViewController.swift
//  UITableViewController
//
//  Created by Родион Шашенко on 01.04.2023.
//

import UIKit

class AddFrendViewController: UIViewController, UITextFieldDelegate {


    @IBOutlet private weak var phoneLabel: UILabel!
    @IBOutlet private weak var sexLabel: UILabel!
    @IBOutlet private weak var ageLabel: UILabel!
    @IBOutlet private weak var nameLable: UILabel!
    
    @IBOutlet private weak var adresTextField: UITextField!
    @IBOutlet private weak var secondNameTextFeild: UITextField!
    @IBOutlet private weak var phoneTextField: UITextField!
    @IBOutlet private weak var emailTextFeild: UITextField!
    @IBOutlet private weak var sexTextFeild: UITextField!
    @IBOutlet private weak var ageTextFeild: UITextField!
    @IBOutlet private weak var nameTextFeild: UITextField!
    
    @IBOutlet private weak var button: UIButton!
    
    var newfrend = Model()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        button.layer.cornerRadius = 15
        secondNameTextFeild.delegate = self
        nameTextFeild.delegate = self
        adresTextField.delegate = self
        emailTextFeild.delegate = self
        phoneTextField.delegate = self
    
    }

    

    
    @IBAction func buttonDidPressed(_ sender: UIButton) {
        defaultTextField(label: nameLable, textField: nameTextFeild)
        defaultTextField(label: ageLabel, textField: ageTextFeild)
        defaultTextField(label: sexLabel, textField: sexTextFeild)
        defaultTextField(label: phoneLabel, textField: phoneTextField)
        
        if nameTextFeild.text != "" && ageTextFeild.text != "" && sexTextFeild.text != "" && phoneTextField.text != ""{
            let tableVC = TableViewController()
            newfrend = Model(firstName: nameTextFeild.text!, secondName: secondNameTextFeild.text ?? "", age: ageTextFeild.text!, sex: sexTextFeild.text!, phoneNumber: phoneTextField.text, workAdress: adresTextField.text ?? "", email: emailTextFeild.text ?? "")
            frendsArray.append(newfrend)
            navigationController?.popViewController(animated: true)
        }
    }
    
    private func defaultTextField(label: UILabel, textField: UITextField){
        if checkTextField(textField: textField, label: label){
            textField.layer.borderWidth = 0.0
            label.textColor = UIColor.darkGray
        }
    }
    private func checkTextField(textField: UITextField, label: UILabel) -> Bool{
       if textField.text == "" {
           textField.layer.masksToBounds = true
           textField.layer.borderColor = UIColor.systemRed.cgColor
           textField.placeholder = "Обязательное поле для заполнения"
           textField.layer.borderWidth = 1.0
           textField.layer.cornerRadius = 10
           label.textColor = UIColor.systemRed
           return false
        }
        return true
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == phoneTextField || textField == emailTextFeild || textField == adresTextField {
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(keyboardWillShow),
                                                   name: UIResponder.keyboardWillShowNotification,
                                                   object: nil)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(keyboardWillHide),
                                                   name: UIResponder.keyboardWillHideNotification,
                                                   object: nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow (notification: NSNotification){
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {return}
        if self.view.frame.origin.y == 0 {
            self.view.frame.origin.y -= keyboardSize.height
        }
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @objc func keyboardWillHide (notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}
