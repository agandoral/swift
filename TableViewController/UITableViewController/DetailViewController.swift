//
//  DetailViewController.swift
//  UITableViewController
//
//  Created by Родион Шашенко on 01.04.2023.
//

import UIKit

class DetailViewController: UIViewController {
    

    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var emailLable: UILabel!
    @IBOutlet weak var phoneLable: UILabel!
    @IBOutlet weak var adressLable: UILabel!
    @IBOutlet weak var sexLable: UILabel!
    @IBOutlet weak var ageLable: UILabel!
    
    var frend: Model!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addFrend()
    }
    
    func addFrend(){
        nameLable.text = (frend.firstName ?? "") + " " + (frend.secondName ?? "")
        emailLable.text = frend.email ?? ""
        phoneLable.text = frend.phoneNumber ?? ""
        ageLable.text = frend.age ?? ""
        sexLable.text = frend.sex ?? ""
        adressLable.text = frend.workAdress ?? ""
        
    }
}
