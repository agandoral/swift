//
//  TableViewController.swift
//  UITableViewController
//
//  Created by Родион Шашенко on 01.04.2023.
//

import UIKit

var frendsArray = [Model]()

class TableViewController: UITableViewController {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Frends"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    print(frendsArray)
        tableView.reloadData()
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return frendsArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? CustomTableViewCell else {return UITableViewCell()}
        
        let model = frendsArray[indexPath.row]
        
        cell.setCell(with: model)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = frendsArray[indexPath.row]
        performSegue(withIdentifier: "showSeque", sender: model)
    }

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        guard segue.identifier == "showSeque" else {return}
        guard let frendModel = sender as? Model else {return}
        
        let destController = segue.destination as! DetailViewController
        destController.frend = frendModel
        
    }
    
}
